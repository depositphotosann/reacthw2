1. Винесіть хедер та футер в окремі компоненти.
2. Винесіть main та пости в окремі компоненти
3. У листі має бути стан "прочитано/не прочитано". Якщо лист не прочитаний, то він відображається у лічильниках у хедері та у футері [https://prnt.sc/26crgs7](https://prnt.sc/26crgs7). На кліку на лист він повинен ставати прочитаним.
   Стилі прочитаного та непрочитаного листа мають відрізнятися.
4. Реалізувати функціонал видалення листа
5. Додати модальне вікно із підтвердженням.

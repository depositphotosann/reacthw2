import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { FaShoppingCart, FaStar } from 'react-icons/fa';

const Header = ({cardCount, favoritesCount}) => {
  return (
    <StyledHeader>
      <h1>Мій Інтернет-магазин</h1>
      <nav>
        <ul>
          <li>
            <FaShoppingCart />
            <span>{cardCount}</span>
          </li>
          <li>
            <FaStar />
            <span>{favoritesCount}</span>
          </li>
        </ul>
      </nav>
    </StyledHeader>
  );
};

const StyledHeader = styled.header`
  position: fixed;
  background-color: #333;
  color: white;
  padding: 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  z-index: 1;
`;

export default Header;